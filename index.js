import Vue from 'vue';
import bm from './view/BackendModules.vue';
import restart from './view/RestartServer.vue';

export default {
  config: {
    verifyBackendModulesUrl: 'eis/ensure_eis_modules',
    backendRestartUrl: 'eis/restart',
    backendDependencies: [
      'development-tools',
    ],
  },
  components: {
    BeModules: bm,
    RestartServer: restart,
  },
  devRouter: (app, options) => ({
    path: options.path,
    component: () => import('./view/DevList.vue'),
    props: () => ({
      GetData: (url, filter = '') => {
        url = url || options.url;
        if (!url) return undefined;
        return Vue.prototype.getRequest(`${url}${filter}`).then((d) => ((d && d.data) ? d.data : {}));
      },
      Options: options,
      Del: (id) => Vue.prototype.deleteRequest(options.url, { id }),
      Add: (obj) => Vue.prototype.postRequest(options.url, obj),
      Edit: (obj) => Vue.prototype.putRequest(options.url, obj),
    }),
  }),
};
